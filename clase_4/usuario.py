class Usuario :
    def __init__(self, pnombre,password,correo):
        self.nombre = pnombre
        self.password = password
        self.correo = correo
    def __str__(self):
        return f"Nombre = {self.nombre}, {self.password} Tel: {self.correo}"
    def getnombre(self):
        return self.nombre
    def getpassword(self):
        return self.password
    def setNombre(self, pNombre):
        self.nombre = pNombre
    def setPassword(self, pPassword):
        self.password = pPassword
    def setCorreo(self, pCorreo):
        self.correo = pCorreo




def agregar():
    nombre = input("Ingrese el Nombre del Usuario: ")
    password = input("Ingrese el Password: ")
    correo = input("Ingrese el Correo: ")
    usuarioNuevo = Usuario(nombre, password, correo)
    listaUsuarios.append(usuarioNuevo)

def informar():
    print(" ")
    print("----Informe----")
    for indice in range(0, len(listaUsuarios)):
        print(f"{indice +1} - {listaUsuarios[indice]}")
def borrar():
    informar()
    indice = int(input("Ingrese el numero de contacto que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaUsuarios[indice -1].getpassword()} {listaUsuarios[indice-1].getnombre()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaUsuarios.remove(listaUsuarios[indice -1])

def modificar():
    informar()
    indice = int(input("Ingrese el numero de contacto que desea modificar: "))
    nombre = input("Ingrese en nuevo nombre de Usuario: ")
    listaUsuarios[indice - 1].setNombre(nombre)
    password = input("Ingrese en nuevo password: ")
    listaUsuarios[indice - 1].setApellido(password)
    correo = input("Ingrese en nuevo Correo: ")
    listaUsuarios[indice - 1].setCorreo(correo)


listaUsuarios = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu--------")
    print ("A - Agregar Usuarios")
    print ("M - Modificar Usuarios")
    print ("L - Lista de Usuarios")
    print ("B - Borrar Usuarios")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'a'):
        agregar()
    elif(opcion == 'l'):
        informar()
    elif(opcion == 'b'):
        borrar()
    elif(opcion == 'm'):
        modificar()
    else :
            print("Opcion Incorrecta")

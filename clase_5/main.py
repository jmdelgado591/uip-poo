from clase_5.herencia import *

if __name__ == '__main__':
    print("Clase #5 ")

    firula = Perro("Don Firula","Perro", 1 )
    firula.informacion()
    firula.alimentar()
    firula.comunicar()

    # cansado = 0
    # while cansado != 1:
    #     firula.saltar()
    #     cansado = int(input("Dejo de saltar {0}?".format(firula.nombre)))
    #     print(type(cansado))

    # Solicitud de informacion
    nom_propietario = input("Ingrese el nombre del propietario ==> ")
    nom_mascota = input("Ingrese el nombre de la mascota ==> ")
    edad =  int(input("Edad de la mascota ==>"))

    # intancia de clase gato
    abril = Gato(nom_propietario, nom_mascota, edad)
    abril.informacion()
    abril.alimentar()
    abril.comunicar()
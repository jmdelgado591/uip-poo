# Clase padre o base
class Animal:
    def __init__(self,nombre, especie, edad):
        self.nombre = nombre
        self.especie = especie
        self.edad = edad
    def alimentar(self):
        pass
    def comunicar(self):
        pass
    def informacion(self):
        print("Soy de la especie {0} y me llamo {1}".format(self.especie, self.nombre))
        print("Soy de tipo ", type(self).__name__)


# Clase hijas o derivada
class Perro(Animal):
    def alimentar(self):
        print("Solamente come pepas y se alimenta tres veces al día")
    def comunicar(self):
        print("Gua! Gua!")
    def saltar(self):
        print("El perro ", type(self).__name__, " Esta Saltando ....")

class Gato(Animal):
    def __init__(self, propietario, nom, edad):
        super().__init__(nom, "Gato", edad)
        self.propietario = propietario
    def informacion(self):
        print("Nombre: ", self.nombre)
        print("Propietario: ", self.propietario)
        print("Especie: ", self.especie)
        print("Representacion de la edad en humano: ", str(self.edad * 6))




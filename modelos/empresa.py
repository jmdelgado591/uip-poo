class Empresa:
    pass

class Empleado:
    """ Clase de empleados de empresa"""
    dni = ""
    nombre = ""
    direccion = ""
    annoEntrada = ""
    fechaNacimento = 0
    salario = ""
    annoActual = 2020

    def __init__(self):
        print("___________CREANDO EMPLEADO_______")
        self.nombre = input("Ingresa su nombre: ")
        self.fechaNacimento = int(input("Fecha de nacimiento: "))
        self.direccion = input("Direccion: ")
        self.dni = input("Cedula: ")

        self.annoEntrada = int (input("Año de entrada: "))
        self.salario = float(input("Salario: ") )

    def annoEmpresa(self):
        return self.annoActual - self.annoEntrada

    def edad(self):
        print("______METODO EDAD_______")
        result = self.annoActual - self.fechaNacimento
        print("Actualmente tienes ===> ", str(result))

    def sueldo(self, numExtra ):
        print("______METODO CALCULANDO SALARIO_______")
        rxh = 35
        resultado = self.salario + (numExtra * rxh)
        return resultado

    def informacion(self):
        print("______METODO INFO_______")
        print("Nombre \t=>\t", self.nombre)
        print("DNI \t=>\t", self.dni)
        print("Direccion \t=>\t", self.direccion)
        print("Salario \t=>\t", self.salario)


if __name__ == "__main__":
    print("Ventana principal clase #3")
    # jose = Empleado()
    # jose.edad()

    # horasExtras = int(input("Cuantas horas extras realiaste este mes ? \n"))
    # print("Cuanto gana Jose: ", jose.sueldo(horasExtras))
    # jose.informacion()

    status = 1
    while status != 0:
        print("PROGRAMA DE INGRESO DE EMPLEADO")
        ob1 =  Empleado()
        ob1.informacion()
        
        status = bool(input("Desea continuar SI=1 / NO=0?"))
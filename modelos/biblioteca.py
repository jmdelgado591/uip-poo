class Biblioteca:
    """ Clase para definir las bibliotecas"""
    nombre = ""
    ubicacion = ""
    n_libros = 0
    telefono = ""
    administrador = ""
    estado = False

    def __init__(self, nom, ubi, n_lib, tel, adm):
        self.nombre = nom
        self.ubicacion = ubi
        self.n_libros = n_lib
        self.telefono = tel
        self.administrador = adm
        self.estado = True
        print("Inicializados los datos...")

    def consultar(self):
        """ Metodo para consultar la infromación de la Biblioteca"""
        print("Nombre ===> ", self.nombre)
        print("Telefono ===> ", self.telefono)
        print("Ubicacion ===> ", self.ubicacion)




def hola():
    print("Hola desde una funcion")
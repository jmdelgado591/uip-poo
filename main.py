from modelos.biblioteca import Biblioteca


if __name__ == "__main__":
    print("Programa Principal ")
    # Crear un objeto a traves de una Instacia

    uip = Biblioteca('UIP', 'Tumba Muerto', 10, '222-90-33', 'Luciam')
    print(uip.__doc__)
    print(uip.administrador)
    print("Cantida de libros ==> ", uip.n_libros)
    uip.consultar()

    b2 = Biblioteca("Jose Dolores", "Zona Libre", 190, "555-00-01", "Maryon Torres")
    b2.consultar()

